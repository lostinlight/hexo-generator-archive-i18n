# hexo-generator-archive-i18n

## Credits

This is an almost identical copy of **hexo-generator-archive-alphabet-i18n** Hexo plugin, with some deletions based on my particular need.
For further info, please, refer to the author's [original repo](https://github.com/xcatliu/hexo-generator-archive-alphabet-i18n).
